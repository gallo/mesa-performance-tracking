#!/usr/bin/python3

# Copyright © 2023 Collabora Ltd.
# SPDX-License-Identifier: MIT

from collections import defaultdict
from dataclasses import dataclass
from datetime import datetime
import os
import re
import threading
from typing import Self

import pandas as pd

from etl.transform.query import DataFrameQueryGenerator
from etl.wrappers.gitlab_connector import GitlabConnector
from etl.wrappers.influxdb_helper import InfluxdbSource

GITLAB_URL: str = os.environ.get("GITLAB_URL", "https://gitlab.freedesktop.org")
GITLAB_PROJECT_PATH: str = os.environ.get("GITLAB_PROJECT_PATH", "mesa/mesa")
GITLAB_TOKEN: str = os.environ.get("GITLAB_TOKEN", "<token unspecified>")

DB_BUCKET: str = "gitlab-stats"
DB_MEASUREMENT_MR: str = "merge-requests-stats"
DB_MEASUREMENT_PIPELINES: str = "streaks-pipelines-stats"
DB_MEASUREMENT_JOBS: str = "finished-jobs-stats"
DB_MEASUREMENT_UNIT_TESTS: str = "unit-tests-flakes-stats"

CONFIG_TRACES_FILE_PATH: str = "./mesa-ci-status/traces_config.yaml"

gl: GitlabConnector = GitlabConnector(GITLAB_PROJECT_PATH)
db_source: InfluxdbSource = InfluxdbSource(DB_BUCKET)
query_generator = DataFrameQueryGenerator(db_source=db_source, query_file="base_query.mako")

lock: threading.Lock = threading.Lock()


@dataclass
class MargeWarning:
    mr_iid: int
    reason: str
    user_project: str
    pipeline: int

    @classmethod
    def sentinel(cls) -> Self:
        return cls(-1, "", "", -1)


def convert_mr_to_pipeline_url(mr_url, pipeline_id):
    # Replace 'merge_requests' with 'pipelines' and append the pipeline ID
    return re.sub(r"/merge_requests/.*$", "/pipelines/", mr_url) + f"{pipeline_id}"


def extract_info_from_marge_warning(mr_iid, body) -> MargeWarning:
    # Regular expression to match the specific pattern
    pattern = re.compile(
        r"I couldn't merge this branch: (?P<reason>.*?)(?= See pipeline|$)(?: See pipeline https://gitlab\.freedesktop\.org/(?P<user_project>[^/]+/[^/]+)/-/pipelines/(?P<pipeline>\d+))?"  # noqa: E501
    )
    match = pattern.search(body)
    if not match:
        raise ValueError(f"Could not match the pattern from the note {body}")

    return MargeWarning(
        mr_iid, match["reason"].strip(), match["user_project"], int(match["pipeline"] or -1)
    )


def get_notes_by_mr(query_generator, since: datetime):
    unix_ts_sec = round(since.timestamp())
    join_query = f"""
import "join"
import "influxdata/influxdb/schema"

mrs =
    from(bucket: "gitlab-stats")
        |> range(start: {unix_ts_sec})
        |> filter(fn: (r) => r._measurement == "gl-mr-stats")
        |> schema.fieldsAsCols()
        |> filter(fn: (r) => r.state == "merged")
        |> filter(fn: (r) => r.target_branch == "main")
        |> group()

notes =
    from(bucket: "gitlab-stats")
        |> range(start: {unix_ts_sec})
        |> filter(fn: (r) => r._measurement == "gl-mr-notes")
        |> schema.fieldsAsCols()
        |> group()

join.inner(
    left: notes,
    right: mrs,
    on: (l, r) => l.mr_id == r.id,
    as: (l, r) =>
        ({{l with
            id: r.id,
            web_url: r.web_url,
            merged_at: r.merged_at,
            project_path: r.project_path,
            title: r.title,
        }}),
)
    |> sort(columns: ["mr_id", "created_at"], desc: false)
    """
    print(join_query)
    notes_by_mr = (
        query_generator.execute_query(join_query)
        .drop_duplicates()
        .set_index("mr_iid", inplace=False)
    )

    return notes_by_mr


def extract_streak_pipelines(notes_by_mr):
    streak_pipelines = []

    def increment_streak_pipelines(mr_iid, note, last_marge_warning):
        streak_pipelines.append(
            (
                mr_iid,
                last_marge_warning.reason,
                note.author_name,
                # note.project_path_marge,
                note.project_path,
                last_marge_warning.pipeline,
                note.web_url,
                note.merged_at,
                note.title,
            )
        )

    last_marge_warning: MargeWarning = MargeWarning.sentinel()
    debug = defaultdict(list)
    for mr_iid, note in notes_by_mr.iterrows():
        assert isinstance(note.body, str)
        if (
            last_marge_warning.mr_iid == mr_iid
            and re.match(r"^added \d+ commits\s", note.body)
            # new commits from author
            and note.author_name != "marge-bot"
        ):
            debug[mr_iid].append(note.body)
            streak_pipelines.pop()
            # New code state: reset streak_pipelines
            last_marge_warning = MargeWarning.sentinel()
            continue
        if note.body.startswith("I couldn't merge") and "Sorry, " not in note.body:
            debug[mr_iid].append(note.body)
            last_marge_warning = extract_info_from_marge_warning(mr_iid, note.body)
            increment_streak_pipelines(mr_iid, note, last_marge_warning)
            continue

    # transform the list of tuples to a DataFrame
    streak_pipelines_per_mr = pd.DataFrame(
        streak_pipelines,
        columns=[
            "mr_id",
            "reason",
            "author_name",
            "project_path",
            "pipeline_id",
            "web_url",
            "merged_at",
            "title",
        ],
    )
    print(streak_pipelines_per_mr.head())
    return streak_pipelines_per_mr


def get_streak_pipeline_jobs(since, streak_pipelines_per_mr):
    """
    Retrieves and processes the jobs associated with streak pipelines.

    Args:
        since (str): The start date for querying the jobs.
        streak_pipelines_per_mr (pd.DataFrame): DataFrame containing streak pipelines per merge request.

    Returns:
        pd.DataFrame: Processed DataFrame with streak pipeline jobs and their retry counts.
    """
    # %% get the jobs from the pipeline ids
    retried_job_per_pipeline_query = (
        query_generator.generate_query(
            DB_BUCKET,
            since,
            "gl-job-stats",
            filters=['r.retried == "True"', 'r.status == "failed"'],
            late_field_cols=True,
        ).rstrip()
        + """
        |> group(columns: ["pipeline_id", "full_name"])
        |> count(column: "retried")
        // Accumulate the number of retried jobs per pipeline
        |> group(columns: ["pipeline_id"])
        |> sum(column: "retried")
        |> rename(columns: {"retried": "#retried_jobs"})
        |> group()
    """
    )
    print(retried_job_per_pipeline_query)
    retried_job_per_pipeline_df = query_generator.execute_query(retried_job_per_pipeline_query)
    print(retried_job_per_pipeline_df.head())
    retried_job_per_pipeline_df.to_csv("/tmp/retried_jobs.csv")

    # %%
    # join the dataframes by pipeline_id
    s_pipelines_table = (
        streak_pipelines_per_mr
        if retried_job_per_pipeline_df.empty
        else (
            pd.merge(
                streak_pipelines_per_mr,
                retried_job_per_pipeline_df,
                on="pipeline_id",
                how="left",
                suffixes=("_mr", "_pipeline"),
            )
            .drop(columns=["table", "result"])
            .reset_index(drop=True)
        )
    )

    if "#retried_jobs" not in s_pipelines_table.columns:
        s_pipelines_table["#retried_jobs"] = 0

    s_pipelines_table["#retried_jobs"] = s_pipelines_table["#retried_jobs"].fillna(0)
    if s_pipelines_table.empty:
        print("No streak pipelines found")
        return s_pipelines_table

    s_pipelines_table["pipeline_url"] = s_pipelines_table.apply(
        lambda row: convert_mr_to_pipeline_url(row["web_url"], row["pipeline_id"]), axis=1
    )
    s_pipelines_table = s_pipelines_table.astype(
        {
            "mr_id": "Int64",
            "merged_at": "string",
            "#retried_jobs": "Int64",
            "web_url": "string",
            "pipeline_url": "string",
        }
    )
    print(s_pipelines_table.head())

    # %%
    return s_pipelines_table


# %%
def main():
    # %%
    since = db_source.get_last_write(
        measurement=DB_MEASUREMENT_MR,
        field="number_of_streaks_pipelines",
        range_start="-3mo",
    )

    print("Start date: ", os.getenv("INFLUXDB_LAST_WRITE", "default"))
    print("Getting MRs merged after", since)

    # %%

    notes_by_mr = get_notes_by_mr(query_generator, since)

    # %%
    # select pair of `added` system notes from non marged-bot authors and
    # `I couldn't merge` system notes from marge-bot
    # but it should be grouped by merge request
    # to get the number of streaks pipelines
    streak_pipelines_per_mr = extract_streak_pipelines(notes_by_mr)

    # %%
    # Get the number of streaks pipelines per merge request
    # agg: count the number of streaks pipelines per merge request
    mr_streak_pipelines = (
        streak_pipelines_per_mr.groupby("mr_id")
        .agg(
            number_of_streaks_pipelines=("pipeline_id", "count"),
            merged_at=("merged_at", "first"),
            web_url=("web_url", "first"),
            project_path=("project_path", "first"),
            title=("title", "first"),
            # reasons=("reason", set),
        )
        .astype({"merged_at": "string", "web_url": "string"})
    )
    mr_streak_pipelines = mr_streak_pipelines.dropna(subset=["merged_at"])
    print(mr_streak_pipelines.head())

    streak_pipe_table = get_streak_pipeline_jobs(since, streak_pipelines_per_mr)

    # %% load the data to influxdb
    with db_source._write_api as writer:
        writer.write(
            bucket=DB_BUCKET,
            record=streak_pipe_table,
            data_frame_measurement_name=DB_MEASUREMENT_PIPELINES,
            data_frame_tag_columns=("reason", "project_path"),
            data_frame_timestamp_column="merged_at",
        )
        writer.write(
            bucket=DB_BUCKET,
            record=mr_streak_pipelines,
            data_frame_measurement_name=DB_MEASUREMENT_MR,
            data_frame_tag_columns=("project_path"),
            data_frame_timestamp_column="merged_at",
        )


# %%
if __name__ == "__main__":
    main()
