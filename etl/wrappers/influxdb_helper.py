#!/usr/bin/python3

# Copyright © 2021 Collabora Ltd.
# SPDX-License-Identifier: MIT

from functools import cached_property
import logging
from dataclasses import dataclass, field
from datetime import datetime, timezone
from os import environ
import re
from typing import Any, Dict, Iterable, Optional, Tuple, TypedDict

from aiohttp_retry import ExponentialRetry, RetryClient


from dateutil.relativedelta import relativedelta
from influxdb_client import (
    InfluxDBClient,
    Point,
    QueryApi,
    WriteApi,
    WriteOptions,
)
from influxdb_client.client.influxdb_client_async import (
    InfluxDBClientAsync,
    WriteApiAsync,
)


class DatePeriodDict(TypedDict, total=False):
    weeks: int
    hours: int
    minutes: int
    months: int
    days: int


def from_range_to_date(range_start: str) -> datetime:
    # Apply range_start to today's date to get the last write
    # extract w==weeks, h==hours, m==minutes
    # Examples:
    # -4w -> 4 weeks ago
    # -4h -> 4 hours ago
    match = re.match(r"-(\d+)(\D)", range_start)
    if not match:
        raise ValueError(f"Invalid range_start value: {range_start}")
    count = int(match[1])
    period = match[2]
    args: dict[str, DatePeriodDict] = {
        "w": {"weeks": count},
        "h": {"hours": count},
        "m": {"minutes": count},
        "mo": {"months": count},
        "d": {"days": count},
    }
    return datetime.now(timezone.utc) - relativedelta(**args[period])


class InfluxdbTokenManager:
    bucket_tokens: dict[str, Optional[str]] = {
        "mesa-perf-v2": environ.get("INFLUX_TOKEN"),
        "marge-stats": environ.get("STATS_INFLUX_TOKEN"),
        "gitlab-stats": environ.get("GITLAB_STATS_INFLUX_TOKEN"),
    }
    fallback_token = "influx-testing-token"

    @staticmethod
    def get_token_from_bucket_name(bucket_name) -> str:
        token = InfluxdbTokenManager.bucket_tokens.get(bucket_name)
        if not token:
            logging.debug(
                "Could not find token for bucket "
                f"{bucket_name}, falling back to "
                f"{InfluxdbTokenManager.fallback_token}."
            )
            return InfluxdbTokenManager.fallback_token

        return token


@dataclass
class InfluxdbSource:
    """Wrapper for influxdb_client library.
    Each instance is fixed to a bucket.
    It also manages which token to use based on the bucket name.

    Args:
        bucket (str): Bucket name
        write_since_weeks (int): How many weeks this source should look in the
                                    past to begin data extraction.
        org (str): Organization name
        url (str): URL location of the influxdb
    """

    _bucket: str = environ.get("INFLUX_BUCKET", "mesa-perf-v2")
    _write_since_weeks: int = 4
    _org: str = environ.get("INFLUX_ORG", "freedesktop")
    _url: str = environ.get("INFLUX_URL", "http://influxdb:8086")
    _class: type[InfluxDBClient] | type[InfluxDBClientAsync] = InfluxDBClient
    _class_kwargs: dict = field(default_factory=lambda: dict(timeout=60_000))
    write_opts = {
        InfluxDBClient: {
            "write_options": WriteOptions(
                # Using a batch size of 5_000, we can get a 413 Request Entity Too Large
                # error. This is a known issue with InfluxDB.
                batch_size=1_000,
                flush_interval=10_000,
                jitter_interval=2_000,
                retry_interval=5_000,
                max_retries=5,
                max_retry_delay=30_000,
                exponential_base=2,
            )
        },
        InfluxDBClientAsync: {},
    }

    def __post_init__(self):
        self._token = InfluxdbTokenManager.get_token_from_bucket_name(self._bucket)
        self._client = self._class(
            url=self._url,
            token=self._token,
            org=self._org,
            **self._class_kwargs,
        )
        self._write_api: WriteApi = self._client.write_api(**self.write_opts[self._class])
        self._query_api: QueryApi = self._client.query_api()
        self.check_server_health()

    def check_server_health(self) -> None:
        if self._class == InfluxDBClientAsync:
            return

        health = self._client.health()
        if health.status == "fail":
            message = (
                f"Could not connect to {self._url} on {self._org}\n"
                "Please verify the token content, if the server is OK and the Influx URL"
            )

            raise ConnectionError(message)

    def write(self, point: Point) -> None:
        self._write_api.write(bucket=self._bucket, org=self._org, record=point)

    def writer_instance(self) -> WriteApi:
        return self._write_api

    @property
    def query_api(self) -> QueryApi:
        return self._query_api

    @staticmethod
    def create_point(name: str) -> Point:
        return Point(name)

    def get_start_date(self) -> datetime:
        """Calculate the time to begin data extraction based on
        write_since_weeks.

        Returns:
            datetime: the date to begin fetching Gitlab results.
        """
        return datetime.now(timezone.utc) - relativedelta(weeks=self._write_since_weeks)

    def _detect_forced_start_date(self) -> Optional[datetime]:
        if forced_last_write := environ.get("INFLUXDB_LAST_WRITE"):
            logging.warning(f"Last write value is hardcoded to {forced_last_write}")
            return datetime.fromisoformat(forced_last_write)
        return None

    def get_last_write(
        self, measurement: str, field: str, range_start: str = "-4w", tags_match={}
    ) -> datetime:
        """Calculate the time to begin data extraction based on the last time
        recorded in the bucket.

        Note: this calculation can be hardcoded via INFLUXDB_LAST_WRITE
        environment variable, set as a ISO format datetime

        Args:
            measurement (str): the name of measurement
            field (str): a field to reduce the query size
            range_start (str): Flux formatted time to be used as range start
                                argument.
            tags_match (dict[str, str]): a dict with tags to match in the query.
                                e.g: {cpu: "cpu0"} -> cpu == "cpu0"

        Returns:
            datetime: the date to begin fetching Gitlab results.
        """

        if forced_start_date := self._detect_forced_start_date():
            return forced_start_date
        return self._get_last_write(measurement, field, range_start, tags_match)

    def _get_last_write(
        self, measurement: str, field: str, range_start: str = "-4w", tags_match={}
    ) -> datetime:
        query = self.get_last_write_query(measurement, field, range_start, tags_match)

        logging.debug(f"Last time query:\n{query}")

        if result := self._query_api.query(query, org=self._org):
            last_write: datetime = result[0].records[0].get_time() + relativedelta(seconds=1)
        else:
            # Fallback to start date
            last_write = self.get_start_date()

        logging.debug(f"Last write result: {last_write}")
        return last_write

    def get_last_write_query(
        self, measurement, field, range_start, tags_match: dict[str, str] = {}
    ):
        tags_str = ""
        if tags_match:
            tags = " and ".join([f'r.{k} == "{v}"' for k, v in tags_match.items()])
            tags_str = f"and {tags}" if tags else ""
        field_str = f'and r._field == "{field}"' if field else ""
        query = f"""from(bucket: "{self._bucket}")
                |> range(start: {range_start})
                |> filter(fn: (r) => r._measurement == "{measurement}"
                    {field_str}
                    {tags_str}
                )
                |> group()
                |> last()
                |> keep(columns: ["_time"])"""
        return query


@dataclass(init=False)
class InfluxdbSourceAsync(InfluxdbSource):
    def __init__(self, *args, **kwargs):
        retry_options = ExponentialRetry(attempts=3)
        super().__init__(
            *args,
            _class=InfluxDBClientAsync,
            _class_kwargs={
                "client_session_type": RetryClient,
                "client_session_kwargs": {
                    "raise_for_status": False,
                    "retry_options": retry_options,
                },
            },
            **kwargs,
        )

    async def ready(self) -> None:
        ready = await self._client.ping()
        logging.info(f"Influxdb Ready: {ready}")

    async def async_get_last_write(
        self, measurement: str, field: str, range_start: str = "-4w", tags_match={}
    ) -> datetime:
        if forced_start_date := self._detect_forced_start_date():
            return forced_start_date
        return await self._async_get_last_write(measurement, field, range_start, tags_match)

    async def _async_get_last_write(
        self, measurement: str, field: str, range_start: str = "-4w", tags_match={}
    ) -> datetime:
        query = self.get_last_write_query(measurement, field, range_start, tags_match)

        logging.debug(f"Last time query:\n{query}")
        query_api = self._client.query_api()

        if result := await query_api.query(query, org=self._org):
            last_write: datetime = result[0].records[0].get_time() + relativedelta(seconds=1)
        else:
            last_write = from_range_to_date(range_start)

        logging.debug(f"Last write result: {last_write}")
        return last_write

    async def async_write(self, record: Point | Iterable[Point], *args, **kwargs) -> None:
        record_bak = list(record) if isinstance(record, Iterable) else [record]
        await self.write_api.write(
            bucket=self._bucket, org=self._org, record=record_bak, *args, **kwargs
        )

    @property
    def client(self) -> InfluxDBClientAsync:
        return self._client

    @cached_property
    def write_api(self) -> WriteApiAsync:
        return self.client.write_api()


@dataclass(frozen=True)
class InfluxdbDictWriter:
    """Helper class to simplify the data input extraction as a dict.

    ```
    tags = ("tag1", "tag2")
    fields = ("field",)
    dw = DictWriter(tags, fields, influx_source)

    input_data = {
        "tag1": 1
        "tag2": 2,
        "field": "abc",
    }

    # insert_data will deal with the complexity of creating a datapoint inside
    # InfluxDB
    dw.insert_data(("point", input_data, time_value)
    """

    __tags: Tuple
    __fields: Tuple
    __source: InfluxdbSource

    def __post_init__(self):
        # Check if tags and fields does not have values in common.
        assert set(self.__tags) & set(self.__fields) == set()

    def insert_data(
        self, measurement: str, input_data: Dict[str, Any], time_value, *args, **kwargs
    ):
        """Insert data into InfluxDB via a validated input dictionary

        Args:
            measurement (str): the name of the point measurement
            input_data (Dict[str, str]): a dict with all of point data, except timing.
            time_value (_type_): the time associated with this point
            *args, **kwargs: arguments sent to point time function
        """
        p = self.__source.create_point(measurement)

        for tag_name in self.__tags:
            p = p.tag(tag_name, input_data[tag_name])

        for field_name in self.__fields:
            p = p.field(field_name, input_data[field_name])

        p = p.time(time_value, *args, **kwargs)

        self.__source.write(p)

    @staticmethod
    def increment_timing(time, frame_no):
        """Use artificial timing to represent re-rendered values.
        This approach is mentioned in the Influx Docs, as a way to deal with duplicated data.
        https://docs.influxdata.com/influxdb/v2.0/write-data/best-practices/duplicate-points/#increment-the-timestamp

        Args:
            time (datetime): the original timing that would be used.
            frame_no (int): the number of the rendering repetition.

        Returns:
            int: the artificial timing inserted.
        """
        timestamp_ms = int(time.timestamp() * 1000)
        # 1 ns = 10e6 ms
        timestamp_ns = timestamp_ms * 1_000_000
        return timestamp_ns + frame_no
