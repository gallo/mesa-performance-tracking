from datetime import datetime
from pandas import DataFrame
from etl.wrappers.influxdb_helper import InfluxdbSource
from mako.lookup import TemplateLookup
from pathlib import Path

# Create a lookup for loading templates
# path to this file with pathlib
DEFAULT_PATH = Path(__file__).parent / "templates"


class DataFrameQueryGenerator:
    def __init__(self, query_file, db_source: InfluxdbSource, template_dir=DEFAULT_PATH):
        self.lookup = TemplateLookup(directories=[template_dir])
        self.query_file = query_file
        self.db_source = db_source

    def generate_query(self, bucket, range, measurement, filters=[], group=[], **kwargs):
        if filters is None:
            filters = []

        if isinstance(range, datetime):
            range = range.strftime("%Y-%m-%dT%H:%M:%SZ")

        params = {
            "bucket": bucket,
            "range": range,
            "measurement": measurement,
            "filters": filters,
            "group": group,
            **kwargs,
        }

        # Load and render the full query template
        template = self.lookup.get_template(self.query_file)
        rendered = template.render(**params)

        if isinstance(rendered, str):
            return rendered

        assert isinstance(rendered, bytes), "Query template did not return a string or bytes"
        return rendered.decode("utf-8")

    def execute_query(self, query, auto_index=True) -> DataFrame:
        df = self.db_source.query_api.query_data_frame(query)
        assert isinstance(df, DataFrame), "Query did not return a DataFrame"
        return df


# Usage example
if __name__ == "__main__":
    client = InfluxdbSource("gitlab-stats", 4, "freedesktop", "http://localhost:8086")
    # Initialize the generator with the path to your templates
    query_generator = DataFrameQueryGenerator(db_source=client, query_file="base_query.mako")

    # Example 1: Marge notes query
    marge_notes_query = query_generator.generate_query(
        bucket="gitlab-stats",
        range="-30d",
        measurement="gl-mr-notes",
        filters=['r.system == "False"', 'r.author_name == "marge-bot"'],
        field_cols=True,
    )
    print("Marge Notes Query:")
    print(marge_notes_query)
    print("\nMarge Notes Results:")
    print(query_generator.execute_query(marge_notes_query))

    # Example 2: Merge requests stats query
    mrs_query = query_generator.generate_query(
        bucket="gitlab-stats", range="-30d", measurement="gl-mr-stats", group=True
    )
    print("\nMerge Requests Stats Query:")
    print(mrs_query)
    print("\nMerge Requests Stats Results:")
    print(query_generator.execute_query(mrs_query))

    # Example 3: Failed jobs query
    failed_jobs_query = query_generator.generate_query(
        bucket="gitlab-stats",
        range="-30d",
        measurement="gl-job-stats",
        filters=['r.status == "failed"'],
        group=True,
    )
    print("\nFailed Jobs Query:")
    print(failed_jobs_query)
    print("\nFailed Jobs Results:")
    print(query_generator.execute_query(failed_jobs_query))
