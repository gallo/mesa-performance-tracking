---
- name: Mesa CI stats
  project:
    dashboards:
    - mesa-ci-stats
    - job-status-per-stage

- default-dashboard: &default-dashboard
    time_options: [1h, 6h, 12h, 24h, 2d, 7d, 14d, 30d]
    refresh_intervals: [5m, 15m, 30m, 1h]
    time:
      from: now-30d
      to: now
    sharedCrosshair: true

- name: mesa-ci-stats
  dashboard:
    title: Mesa CI stats
    <<: *default-dashboard

    rows:
    - row:
        panels:
        - graph:
            title: "Daily job failures in Marge pipelines"
            datasource: &datasource InfluxDB-CI-Stats
            legend:
              show: false
            targets:
                - influxdb-target:
                    query: 'from(bucket: "marge-stats")
                            |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
                            |> filter(fn:(r) => r._measurement == "pipeline_failure")
                            |> drop(columns: ["pipeline"])
                            |> aggregateWindow(every: 1d,fn: count)'
        - graph:
            title: "Number of failures for each job for the given period"
            datasource: *datasource
            bars: true
            lines: false
            xaxis:
              mode: series
              show: false
              values:
                - "total"
            legend:
              show: true
              total: true
              alignAsTable: true
              values: true
            tooltip:
              shared: false
              sort: 0
              value_type: "individual"
            targets:
                - influxdb-target:
                    query: 'from(bucket: "marge-stats")
                            |> range(start: v.timeRangeStart, stop: v.timeRangeStop)
                            |> filter(fn:(r) => r._measurement == "job_failure")
                            |> drop(columns: ["pipeline", "job_stage"])'

- name: job-status-per-stage
  dashboard:
    title: Marge triggered job status per stage
    <<: *default-dashboard

    templates:
    - query:
        name: stage
        label: Stage
        datasource: &mesa_stats InfluxDB-CI-Stats
        includeAll: true
        multi: true

        query: |
          import "influxdata/influxdb/schema"
          schema.tagValues(
              bucket: "marge-stats",
              tag: "stage",
          )

    - query:
        name: status
        label: Status
        datasource: *mesa_stats

        query: |
          import "influxdata/influxdb/schema"
          schema.tagValues(
              bucket: "marge-stats",
              tag: "status",
          )

    rows:
    - row:
        panels:
        - graph:
            title: "Job ${status} ratio in ${stage}"
            repeat: stage
            datasource: *mesa_stats
            yaxes:
              - yaxis:
                  format: percent
                  min: 0
                  max: 100
              - yaxis:
                  show: false
            legend:
              show: false
            tooltip:
              shared: false
              sort: 0
              value_type: "individual"
            targets:
                - influxdb-target:
                    query: |
                      percent = (sample, total) => float(v: sample) / float(v: total) * 100.0

                      aggregateCount = (tables=<-) => tables
                          |> aggregateWindow(every: v.windowPeriod, fn: count, createEmpty: false)

                      job_status_percentage = (status) => {{
                          base = from(bucket: "marge-stats")
                          |> range(start: v.timeRangeStart)
                          |> filter(fn: (r) => r._measurement == "job")
                          |> filter(fn: (r) => r._field == "pipeline_id")
                          |> filter(fn: (r) => r.stage == "${stage}")
                          |> group(columns: ["pipeline_id", "stage"])

                          total = base
                              |> aggregateCount()

                          sample = base
                              |> filter(fn: (r) => r.status == status, onEmpty: "keep")
                              |> aggregateCount()

                          status_ratio = join(tables: {{total: total, sample:sample}}, on: ["stage", "_time", "_stop", "_start"])
                              |> map(fn: (r) => ({{ r with ratio: percent(sample: r._value_sample, total: r._value_total) }}))
                              |> keep(columns: ["_time", "ratio", "_start", "_stop"])

                          return status_ratio
                      }}

                      job_status_percentage(status: "${status}")