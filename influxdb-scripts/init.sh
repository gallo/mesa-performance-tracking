#!/bin/bash
set -ex

influx bucket create -n marge-stats -r 4w
# Get one week more to be able to get the window for metrics M, such as "M for the last D days"
influx bucket create -n gitlab-stats -r 5w
